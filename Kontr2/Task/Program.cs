﻿using System.Text;
using System;
using System.Collections.Generic;
using System.Diagnostics;


class Node
{
    public int key;
    public string value;
    public Node next;
}

class HashTable
{
    private int size;
    private Node[] table;

    public HashTable(int size)
    {
        this.size = size;
        table = new Node[size];
    }

    private int HashFunction(int key)
    {
        return key % size;
    }

    public void Add(int key, string value)
    {
        int index = HashFunction(key);
        Node newNode = new Node();
        newNode.key = key;
        newNode.value = value;

        if (table[index] == null)
        {
            table[index] = newNode;
        }
        else
        {
            Node current = table[index];
            while (current.next != null)
            {
                current = current.next;
            }
            current.next = newNode;
        }
    }

    public bool Remove(int key)
    {
        int index = HashFunction(key);

        if (table[index] == null)
        {
            return false;
        }

        if (table[index].key == key)
        {
            table[index] = table[index].next;
            return true;
        }

        Node current = table[index];
        while (current.next != null)
        {
            if (current.next.key == key)
            {
                current.next = current.next.next;
                return true;
            }
            current = current.next;
        }

        return false;
    }

    public string Search(int key)
    {
        int index = HashFunction(key);

        if (table[index] == null)
        {
            return null;
        }

        Node current = table[index];
        while (current != null)
        {
            if (current.key == key)
            {
                return current.value;
            }
            current = current.next;
        }

        return null;
    }

    public void Print()
    {
        for (int i = 0; i < size; i++)
        {
            Node current = table[i];
            while (current != null)
            {
                Console.WriteLine("Ключ: {0}, Значення: {1}", current.key, current.value);
                current = current.next;
            }
        }
    }

    public int Count()
    {
        int count = 0;
        for (int i = 0; i < size; i++)
        {
            Node current = table[i];
            while (current != null)
            {
                count++;
                current = current.next;
            }
        }
        return count;
    }
}

class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = UTF8Encoding.UTF8;
        Stopwatch sw = new Stopwatch();
        sw.Start();
        HashTable hashTable = new HashTable(6);

        hashTable.Add(1, "А");
        hashTable.Add(2, "Б");
        hashTable.Add(3, "В");
        hashTable.Add(4, "Г");
        hashTable.Add(5, "Д");
        


        
        Console.WriteLine("HashTable:");
        hashTable.Print();
        Console.WriteLine("\nКількість елементів у HashTable: {0}", hashTable.Count());
        sw.Stop();
        Console.WriteLine("Витрачений час: {0}", sw.Elapsed);

        
        
        Console.Write("Введіть ключ для видалення з таблиці:");
        int keyToRemove = int.Parse(Console.ReadLine());
        if (hashTable.Remove(keyToRemove))
        {
            Console.WriteLine("\nКлюч {0} був видаленний з HashTable.", keyToRemove);
            Console.WriteLine("\nКількість елементів у HashTable: {0}", hashTable.Count());
            Console.WriteLine("Нова HashTable");
            hashTable.Print();
        }
        else
        {
            Console.WriteLine("\nКлюч {0} не був знайдений в HashTable.", keyToRemove);
        }

        Console.Write("Введіть ключ для пошуку в таблиці:");
        int keyToSearch = int.Parse(Console.ReadLine());
        string value = hashTable.Search(keyToSearch);
        if (value != null)
        {
            Console.WriteLine("\nКлюч {0} був знайдений в HashTable з значенням: {1}.", keyToSearch, value);
        }
        else
        {
            Console.WriteLine("\nКлюч {0} не був знайдений в HashTable.", keyToSearch);
        }
    }
}